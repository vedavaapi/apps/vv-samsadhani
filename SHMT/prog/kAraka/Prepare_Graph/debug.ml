(* debugging context building *)

#load "../ZEN_3.2/list2.cmo";
#load "../ZEN_3.2/gen.cmo";
#load "../ZEN_3.2/word.cmo";
#load "../ZEN_3.2/trie.cmo";
#load "../ZEN_3.2/zen_lexer.cmo";
#load "../Heritage_ML/transduction.cmo";
#load "../ZEN_3.2/ascii.cmo";
#load "bank_lexer.cmo";
#load "clips.cmo";

open List2;
open Gen;
open Word;
open Trie;
open Zen_lexer;
(* open Transduction; *)
open Ascii;
open Clips;

